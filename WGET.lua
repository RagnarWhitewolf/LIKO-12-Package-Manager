--[[ Simple webget lua library for LIKO-12
      Usage:
       wget.download(address,file) download the address to the file
       wget.downVar(address) download the file and return the file
]]--
--[[ ERROR CODES:
	  nil = NO DATA IN THE URL
	  1   = ERROR OF HTTP
	  2   = NO WEB PERIPHERAL FOUND
]]--
wget = {}

function wget.download(address,filename)
  local trm = require("terminal")
  if not WEB then
    return 2, "NO WEB PERIPHERAL!"
  else
    local body, data, data2 = http.request(address)
    if data then
      if tonumber(data.code) ~= 200 then
        return 1, "ERROR! HTTP:" .. data.code -- BUG HERE, FAIL IN FILE DOWNLOAD, NIL VALUE EXCEPT IN FILE CONTENT
      else
        if not filename then
          local tokens = split(address, '/')
          filename = tokens[#tokens]
        end
        fs.write(filename,body)
      end
      return 0
    else
      return nil, "NO DATA!"
    end
  end
end

function wget.downVar(address)
    local trm = require("terminal")
  if not WEB then
    return 2, "NO WEB PERIPHERAL!"
  else
    local body, data, data2 = http.request(address)
    if data then
      if tonumber(data.code) ~= 200 then
        return 1, "ERROR! HTTP:".. data.code
      else
        local file = body
      end
      return file
    else
      return nil, "NO DATA!"
    end
  end
end

function wget.test(address)
 local body, data, data2 = http.request(address)
 if data then
  if tonumber(data.code) ~= 200 then
   local check = false
  elseif tonumber(data.code) == 200 then
   local check = true
  end
 end
 return check
end

return wget