--[[  Liko-12 Package Manager Core Library

      Information:
        This library is the core of lpkg.
      Usage:
        lpkg.changelog() -- THIS WILL DISPLAY THE LPKG CHANGELOG
        LPKG:
          lpkg.pkg.help(cmdName) -- PRINTS THE USAGE OF THE PROGRAM
          lpkg.pkg.install(packagefile,printlog) -- INSTALL THE PACKAGE FROM THE SPECIFIED FILE, IF PRINTLOG IS TRUE, LPKG WILL PRINT THE STATE DIALOGS
          lpkg.pkg.uninstall(package,printlog) -- UNINSTALL THE GIVEN PACKAGE, IF THE PRINTLOG OPTION ARE TRUE, THE PROGRAM WILL PRINT THE STATE DIALOGS
          lpkg.pkg.list() -- RETURNS A TABLE WITH THE CURRENT INSTALLED PACKAGES
          lpkg.pkg.manual(package,printlog) -- DISPLAY THE MANUAL OF A PACKAGE, IF PRINTLOG ARE TRUE, PRINTS THE STATE DIALOGS
          lpkg.pkg.license(package,printlog) -- DISPLAY THE LICENSE OF A PACKAGE, IF PRINTLOG ARE TRUE, PRINTS THE STATE DIALOGS
        LPGET:
          lpkg.get.help(cmdName) -- PRINTS THE USAGE OF THE PROGRAM
          lpkg.get.install(packagename) -- DOWNLOAD AND INSTALLS A PACKAGE
          lpkg.get.getRepositoryAppList(repositoryname,printlog) -- RETURNS THE AVAILABLE PACKAGES IN A REPOSITORY
          lpkg.get.getRepositoryListing(printlog) -- RETURNS THE LIST OF REPOSITORIES INSTALLED IN THE SYSTEM
          lpkg.get.addRepository(repositoryName,repositoryUrl,printLog) -- ADD A REPOSITORY IN THE SYSTEM
          lpkg.get.deleteRepository(repositoryName,printLog) -- DELETES A REPOSITORY INSTALLED IN THE SYSTEM
]]--

lpkg = {}

function lpkg.changelog() -- VIEW CHANGELOG FUNCTION
  local term = require("terminal") -- LOAD THE "terminal" LIBRARY
  term.execute("edit","C:/lpkg/changelog.txt")
end

------------------------------------------------------------------------------
-- LPKG FUNCTIONS: -----------------------------------------------------------
------------------------------------------------------------------------------

lpkg.pkg = {}

function lpkg.pkg.help(cmdName) -- HELP FUNCTION
  if cmdName == nil then
    cmdName = "lpkg"
  end
  color(12);print("L",false);color(8);print("I",false);color(11);print("K",false);color(4);print("O",false);color(7);print("-",false);color(9);print("1",false);color(15);print("2",false);color(7);print(" PACKAGE MANAGER 1.5.0")
  color(9); print("USAGE:"); color(7)
  color(6);print("  "..cmdName.." install <packagefile>")
  color(7);print("  "..cmdName.." uninstall <packagename>")
  color(6);print("  "..cmdName.." list")
  color(7);print("  "..cmdName.." manual <packagename>")
  color(6);print("  "..cmdName.." license <packagename>")
  color(7);print("  "..cmdName.." changelog")
  
  color(7)
end

function lpkg.pkg.install(packagefile,printlog) -- INSTALL PACKAGE FUNCTION
  local term = require("terminal") -- LOAD THE "terminal" LIBRARY
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  dofile("C:/lpkg/CORE.PKGDB.lua") -- LPKG DATABASE LIBRARY
  
  local path = term.resolve(packagefile)

 if fs.exists(path) == true then
  if printlog then
    color(9); print("[WARN] INSTALLING FILE"); color(7)
  end
  lpfs.mount(path)
  local cpath = term.getpath()
  lpfs.cd("ZIP:")
  term.execute("_INSTALL")
  lpfs.cd(cpath)

  if pVerNo ~= nil then
   count = 1
   
   while true do
    lpfs.copy(fileList[count],filePath[count])
    count = count + 1

    if count >= #fileList then
     local tmpPath = "C:/lpkg/"
     if fs.exists("ZIP:/_UNINSTALL.lua") == true then
      lpfs.copy("ZIP:/_UNINSTALL.lua",tmpPath.."uninstall/"..pName) -- IF UNINSTALL FILE EXISTS, COPY TO THE UNINSTALL FILES FOLDER
     end

     if fs.exists("ZIP:/_LICENSE.txt") == true then
      lpfs.copy("ZIP:/_LICENSE.txt",tmpPath.."license/"..pName) -- IF HAVE LICENSE, COPY TO THE LICENSE FOLDER
     end

     if fs.exists("ZIP:/_MANUAL.txt") == true then
      lpfs.copy("ZIP:/_MANUAL.txt",tmpPath.."manual/"..pName) -- IF HAVE MANUAL, COPY IT TO THE MANUAL FOLDER
     end
     if pVerNo < 5 then
       pVersion = {0,0,0}
       pDesc = "No description provided, because of old package format detected."
     end
     lpkgdb.add(pName,pVersion,pDesc,filePath) -- ADD AN ENTRY IN THE PROGRAM DATABASE WITH THE PACKAGE INFORMATION
	   lpfs.mount(nil)
     pName = nil
	   pVerNo = nil
     fileList = nil
     filePath = nil
     break
    end
  end

  elseif pVerNo == nil then -- COMPATIBILITY FOR THE OLDER VERSIONS OF LPKG
   lpfs.mount(nil)
  end
  if printlog then
    color(11); print("[OK] PACKAGE INSTALLED"); color(7)
  end

 elseif fs.exists(path) == false then
  if printlog then
    color(8) print("FILE DON'T EXISTS!") color(7)
  end
 end
end

function lpkg.pkg.uninstall(package,printlog) -- UNINSTALL FUNCTION
  local term = require("terminal") -- LOAD THE "terminal" LIBRARY
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  dofile("C:/lpkg/CORE.PKGDB.lua") -- LPKG DATABASE LIBRARY

  local packageToUninstall = package
-- CHECK AND EXECUTE THE UNINSTALL FILE OF THE PACKAGE:
if fs.exists("C:/lpkg/uninstall/"..packageToUninstall) == true then -- CHECK IF THE PACKAGE EXISTS
  lpfs.copy("C:/lpkg/uninstall/"..packageToUninstall,"C:/lpkg/uninstall/"..packageToUninstall..".lua")
  lpfs.rem("C:/lpkg/uninstall/"..packageToUninstall)
  dofile("C:/lpkg/uninstall/"..packageToUninstall..".lua")
  
  if printlog then
    color(9); print("[WARN] DELETING \""..packageToUninstall.."\" PACKAGE"); color(7)
  end
  
  if pVerNo ~= nil then
   count = 1

   while true do
    lpfs.rem(fileList[count])
    count = count + 1

    if count >= #fileList then
      lpkgdb.delete(package)
	    pVerNo = nil
      fileList = nil
      lpfs.rem("C:/lpkg/uninstall/"..packageToUninstall..".lua")
      break
    end
   end


  elseif pVerNo == nil then -- SUPPORT FOR OLDER VERSIONS
   if printLog == true then
    print("")
   end
   if fs.exists(term.getpath()..packageToUninstall..".lua") == true then
    lpfs.rem("C:/lpkg/uninstall/"..packageToUninstall..".lua")
   end
   print("")
  end

  if fs.exists("C:/lpkg/manual/"..packageToUninstall) == true then
   lpfs.rem("C:/lpkg/manual/"..packageToUninstall)
  end

  if fs.exists("C:/lpkg/license/"..packageToUninstall) == true then
   lpfs.rem("C:/lpkg/license/"..packageToUninstall)
  end
  if printlog then
    color(11); print("[OK] PACKAGE \""..packageToUninstall.."\" DELETED"); color(7)
  end

 else
  if printlog then
    color(8); print("The package "..packageToUninstall.." not exist!"); color(7)
  end
 end
end

function lpkg.pkg.list() -- LPKG PACKAGE LISTING FUNCTION
  dofile("C:/lpkg/CORE.PKGDB.lua") -- LPKG DATABASE LIBRARY
  
  return lpkgdb.apps()
end

function lpkg.pkg.manual(package,printlog) -- MANUAL VIEWER FUNCTION
 local term = require("terminal") -- LOAD THE "terminal" LIBRARY
 if fs.exists("C:/lpkg/manual/"..package) == true then
  term.execute("edit","C:/lpkg/manual/"..package)

 else
  if printlog then
    color(8); print("The package "..package.." not exist or don't have any manual!")
  end
 end
end

function lpkg.pkg.license(package, printlog) -- LICENSE VIEWER FUNCTION
 local term = require("terminal") -- LOAD THE "terminal" LIBRARY
 if fs.exists("C:/lpkg/license/"..package) == true then
  term.execute("edit","C:/lpkg/license/"..package)
 else
  if printlog then
    color(8); print("The package "..package.." not exist or don't have any license!")
  end
 end
end

------------------------------------------------------------------------------------------------------
-- LPGET FUNCTIONS: ----------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
lpkg.get = {}

function lpkg.get.help(cmdName) -- PRINT HELP DIALOG:
 if cmdName == nil then
   cmdName = "lpget"
 end
 color(12);print("L",false);color(8);print("I",false);color(11);print("K",false);color(4);print("O",false);color(7);print("-",false);color(9);print("1",false);color(15);print("2",false);color(7);print(" GET PACKAGE 1.5.0")
 color(9); print("USAGE:"); color(7)
 color(6);print("  "..cmdName.." install <packagename>")
 color(7);print("  "..cmdName.." list <reponame>")
 color(6);print("  "..cmdName.." list repo")
 color(7);print("  "..cmdName.." add repo <name> <url>")
 color(6);print("  "..cmdName.." delete repo <name>")

 color(7)
end

function lpkg.get.install(packagename,printlog) -- PACKAGE INSTALL FUNCTION:
  local term = require("terminal") -- LOAD THE "terminal" LIBRARY
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  dofile("C:/lpkg/lib/wget.lua") -- LOAD THE "wget" LIBRARY
  
  lpfs.copy("C:/lpkg/repo.cfg","C:/lpkg/repo.lua")
  dofile("C:/lpkg/repo.lua")
  lpfs.rem("C:/lpkg/repo.lua")
 
  local tmpCOUNTlim = #repoName
  tmpCOUNTstr = 1

 while true do
  if repoURL[tmpCOUNTstr] ~= nil then
   wget.download(repoURL[tmpCOUNTstr]..packagename..".lpkg","C:/lpkg/"..packagename..".lpkg")
  else
   tmpCOUNTstr = tmpCOUNTstr + 1
  end
  if fs.exists("C:/lpkg/"..packagename..".lpkg") == true then
   break
  end
  if tmpCOUNTstr >= tmpCOUNTlim then
   break
  end
  tmpCOUNTstr = tmpCOUNTstr + 1
 end
 
 lpkg.pkg.install("C:/lpkg/"..packagename..".lpkg",true)
 lpfs.mount("C:/lpkg/"..packagename..".lpkg")
 local cpath = term.getpath()
 lpfs.cd("ZIP:")
 term.execute("_INSTALL")
 lpfs.cd(cpath)

 if pDepend == true then -- CHECKING DEPENDENCIES
  count = 1

  while true do
    lpkg.get.install(Dependency[count],true)
    count = count + 1

    if count >= #Dependency then
      pDepend = nil
      break
    end
  end
 end
 lpfs.rem("C:/lpkg/"..packagename..".lpkg")

 lpfs.mount(nil)
 pName = nil
 pVerNo = nil
 fileList = nil
 filePath = nil
end

function lpkg.get.getRepositoryAppList(repositoryname,printlog) -- REPOSITORY APP LISTING:
  local term = require("terminal") -- LOAD THE "terminal" LIBRARY
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  dofile("C:/lpkg/lib/wget.lua") -- LOAD THE "wget" LIBRARY
  
  lpfs.copy("C:/lpkg/repo.cfg","C:/lpkg/repo.lua")
  dofile("C:/lpkg/repo.lua")
  lpfs.rem("C:/lpkg/repo.lua")


 local tmpCOUNTlim = #repoName
 tmpCOUNTstr = 1

 while true do
  fresult = repoName[tmpCOUNTstr]

  if fresult == repositoryname then
   tmpURL = repoURL[tmpCOUNTstr]
   search = true
   break
  end

  if tmpCOUNTstr >= tmpCOUNTlim then
   if printLog == true then
    color(8);print("REPOSITORY NOT FOUND!"); color(7)
   end
   local search = false
   break
  end
  tmpCOUNTstr = tmpCOUNTstr + 1
 end

 if search == true then
  wget.download(tmpURL.."_LIST","C:/lpkg/tmp")
  term.execute("edit","C:/lpkg/tmp")
  lpfs.rem("C:/lpkg/tmp")
 end
end

function lpkg.get.getRepositoryListing(printlog) -- GET THE LIST OF INSTALLED REPOSITORIES:
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  
  lpfs.copy("C:/lpkg/repo.cfg","C:/lpkg/repo.lua")
  dofile("C:/lpkg/repo.lua")
  lpfs.rem("C:/lpkg/repo.lua")
  
 local tmpCOUNTlim = #repoName
 tmpCOUNTstr = 1
 print("REPOSITORY LIST:")
 print("")

 while true do

  if repoName[tmpCOUNTstr] ~= nil then
   print(repoName[tmpCOUNTstr])
  end

  if tmpCOUNTstr >= tmpCOUNTlim then
   break
  end
  tmpCOUNTstr = tmpCOUNTstr + 1
 end
 print("")

end

function lpkg.get.addRepository(repositoryName,repositoryUrl,printLog) -- ADDS A REPOSITORY IN THE DATABASE:
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY
  
  lpfs.copy("C:/lpkg/repo.cfg","C:/lpkg/repo.lua")
  dofile("C:/lpkg/repo.lua")
  lpfs.rem("C:/lpkg/repo.lua")
  
 local cfg = fs.read("C:/lpkg/repo.cfg")
 local nid = #repoName + 1
 fs.write("C:/lpkg/repo.cfg",cfg.."\n\nrepoName["..nid.."] = \""..repositoryName.."\"\nrepoURL["..nid.."]  = \""..repositoryUrl.."\"")
 if printLog == true then
  color(11) print("ADDED!") color(7)
 end
end

function lpkg.get.deleteRepository(repositoryName,printLog) -- DELETES THE GIVEN REPOSITORY:
  dofile("C:/lpkg/lib/lpfs.lua") -- LOAD THE "lpfs" LIBRARY

  lpfs.copy("C:/lpkg/repo.cfg","C:/lpkg/repo.lua")
  dofile("C:/lpkg/repo.lua")
  lpfs.rem("C:/lpkg/repo.lua")
  
 local count = #repoName
 local del   = repositoryName
 local file  = fs.read("C:/lpkg/repo.cfg")
 counl = 1

 while true do
  if repoName[counl] == del then
   local newFileP1 = string.gsub(file,"\""..repoName[counl].."\"","nil")
   local newFile   = string.gsub(newFileP1,"\""..repoURL[counl].."\"","nil")
   fs.write("C:/lpkg/repo.cfg",newFile)
   if printLog == true then
    color(11) print("DELETED!")color(7)
   end
   break
  end

  if counl >= count then
   if printLog == true then
    color(8) print("REPOSITORY \""..repositoryName.."\" NOT FOUND!") color(7)
   end
   break
  end
  counl = counl + 1
 end
end

return lpkg