local args = {...} -- COLLECT THE ARGUMENTS

dofile("C:/lpkg/CORE.lua") -- LOAD THE CORE LIBRARY
---------------------------------
if #args == 2 and args[1] == "install" then -- BEGIN INSTALLING PROCESS
  lpkg.pkg.install(args[2],true)
elseif #args == 2 and args[1] == "uninstall" then -- UNINSTALL MODE
  lpkg.pkg.uninstall(args[2],true)
elseif #args == 1 and args[1] == "list" then -- PACKAGE LISTING
  local listing = lpkg.pkg.list()
  
  for i = 1, #listing, 1 do
    print(listing[i].."    ")
  end
elseif #args == 2 and args[1] == "manual" then -- MANUAL VIEWER
  lpkg.pkg.manual(args[2],true)
elseif #args == 2 and args[1] == "license" then -- LICENSE VIEWER
  lpkg.pkg.license(args[2],true)
elseif args[1] == "changelog" then -- VIEW THE CHANGELOG OF LPKG
  lpkg.changelog()
else
  color(8); print("UNKNOWN COMMAND!"); color(7)
elseif #args < 1 or args[1] == "-h" or "--help" or "-?" then -- HELP SCREEN
  lpkg.pkg.help("lpkg")
end