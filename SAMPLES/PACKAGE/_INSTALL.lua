-- INFO:
pName    = "package name"        -- PACKAGE NAME
pVersion = {1,0,0}               -- PACKAGE VERSION
pDesc    = "package description" -- PACKAGE DESCRIPTION
pUpdate  = "updates url"         -- PACKAGE UPDATE URL !WIP!
pVerNo   = 5                     -- PACKAGE FORMAT VERSION (THE VERSION 5 IS THE LATEST VERSION)
pDepend  = true                  -- TRUE OR FALSE IF THE PACKAGE HAS DEPENDENCIES
--------
pDir    = "ZIP:/"
-- FILES:
fileList = {}
filePath = {}
Dependency = {}          -- IF THE VARIABLE "pDepend" IS FALSE, THIS VARIABLE IS USELESS

fileList[1] = pDir.."sample.lua"       -- THE PROGRAM FILE
filePath[1] = "C:/Programs/sample.lua" -- THE PATH TO INSTALL

Dependency[1] = "mines"                      -- A SIMPLE DEPENDENCY PACKAGE, LPKG WILL DOWNLOAD AND INSTALL IT WITH THIS PACKAGE
