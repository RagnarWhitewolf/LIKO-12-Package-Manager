# LIKO-12 PACKAGE MANAGER FILE TUTORIAL:
1. Download all of this folder and edit all the files (the code are commented) for create the package file.
2. If you have finished editing, pack the files in a zip file and change the .zip extension to .lpkg extension.
3. Test your package in LIKO-12 and share it with the world.

## NOTE:
Don't change the names of the files: _INSTALL.lua, _UNINSTALL.lua, _LICENSE.txt and _MANUAL.txt.
If you change the name of any of these files, lpkg will ignore it during the install.
