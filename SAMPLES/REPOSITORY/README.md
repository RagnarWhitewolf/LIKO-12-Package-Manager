# HOW-TO SETUP A REPOSITORY:
1. Create a web hosting accesible to everyone or a GitHub repository.
2. In the folder that you want to put your repository create a file called `_LIST`.
3. In the file `_LIST` put the package list like this:

  `package1 ... Description`
  
  `package2 ... Description`
  
  `package3 ... Description`

4. Upload the package files with the `.lpkg` extension and with the same name of the same package in the list.
5. Test the repository and if the repository works correctly you can share your repository.
