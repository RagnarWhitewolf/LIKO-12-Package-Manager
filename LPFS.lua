--[[ Simple filesystem handling library for LPKG
      Usage:
       lpfs.copy(file,dest) copy file to destination
       lpfs.rem(file) removes file or folder
       lpfs.mount(file) mount the file, if the given file is nil umount the file
       lpfs.cd(dir) change the current working dir to the directory specified
]]--

lpfs = {}

function lpfs.copy(fil,dest)
 local function copyRecursive(from, to, verbose)
  color(10)
  if verbose then
    sleep(0)
  end
  if fs.isDirectory(from) then
    if fs.exists(to) then
      to = fs.combine(to, fs.getName(from))
    end
    local files = fs.getDirectoryItems(from)
    for _,file in ipairs(files) do
      copyRecursive(
        fs.combine(from, file),
        fs.combine(to, file),
        verbose
      )
    end
  else
    local data = fs.read(from)
    fs.write(to, data)
  end
 end

 local term = require("terminal")
 local source = term.resolve(fil)
 local destination = term.resolve(dest)

 color(8)

 if not fs.exists(source) then return 1, "!SRC!" end
 if fs.isReadonly(destination) then return 1, "!ROM!" end

 --Create destination folders
 --Parse directories in the destination
 local d, p = destination:match("(.+):/(.+)") --C:/./Programs/../Editors
 p = ("/"..p.."/"):gsub("/","//"):sub(2,-1)
 local dp, dirs = "", {}
 for dir in string.gmatch(p,"/(.-)/") do
  dp = dp.."/"..dir
  table.insert(dirs,dp)
 end

 --Create the missing directories
 for k, dir in ipairs(dirs) do
  if (not fs.exists(dir)) and not (fs.isFile(source) and k == #dirs) then
    if not pcall(fs.newDirectory,dir) then return 1, "!CFF!" end
  elseif fs.exists(dir) and fs.isFile(dir) then
    if k < #dirs then
      return 1, "!CIF!"
    elseif fs.isDirectory(source) then
      return 1, "!CDF!"
    end
  end
 end

 --Copy the source !
 copyRecursive(source, destination, verbose)
 color(11)
 sleep(0)
end

function lpfs.rem(fil)
 local term = require("terminal")

 local function index(path,notfirst)
  color(9)
  if fs.isFile(path) then sleep(0) fs.delete(path) return end
  local items = fs.getDirectoryItems(path)
  for k, item in ipairs(items) do
    if fs.isDirectory(path..item) then
      sleep(0) pullEvent()
      index(path..item.."/",true)
    else
      sleep(0) pullEvent()
      fs.delete(path..item)
    end
  end
  local isDir = fs.isDirectory(path)
  fs.delete(path)
  if not notfirst then sleep(0) end
  return true
 end

 tar = term.resolve(fil)

 if not fs.exists(tar) then return 1, "!EXI!" end
 if fs.isReadonly(tar) then return 1, "!ROM!" end

 index(fs.isFile(tar) and tar or tar.."/")
end

function lpfs.cd(dir)
 local trm = require("terminal")
 trm.setpath(dir)
end

function lpfs.mount(pkg)
 local trm = require("terminal")
 if pkg ~= nil then
  local path = trm.resolve(pkg)
  local zip = fs.read(path)
  fs.mountZIP(zip)
 elseif pkg == nil then
  fs.mountZIP()
 end
end

return lpfs