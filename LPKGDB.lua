--[[  Liko-12 Package Manager Data Base Library

		Information:
		  This Library provides functions for managing the database of programs of lpkg,
		  The library have been integrated in lpkg since the 1.5.0.
		Usage:
		  lpkgdb.build() -- Builds the database and gives it the lpkg and lpget entry.
		  lpkgdb.apps() -- Returns a list with the available programs in the system.
		  lpkgdb.filelist(package) -- Returns a list with the full path of all of the files of a program.
		  lpkgdb.getver(package) -- Returns a table with the version of the provided package.
		  lpkgdb.getdesc(package) -- Returns a string with the description of the package.
		  lpkgdb.delete(package) -- Removes a package of the database.
		  lpkgdb.add(name,version,description,files) -- Adds a database entry with the provided information.
]]--

lpkgdb = {}

function lpkgdb.build()
  local DatabaseFile = "C:/lpkg/program.db"
  local dbHeader = "-- LPKG PROGRAM DATABASE\nPROGRAMS = {}\n------"
  local lpkgHeader = "-- LPKG INFO:\nPROGRAMS[1] = {\"lpkg-CORE\",{1,5,0},\"Liko-12 Package Manager\",{\"C:/lpkg/\",\"C:/Programs/System/lpkg.lua\",\"C:/Programs/System/lpget.lua\"}}\n-- PROGRAMS:"
  
  fs.write(DatabaseFile, dbHeader.."\n"..lpkgHeader.."\n")
end

function lpkgdb.apps()
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- NAME PARSING:
  tempNames = {}
  for i = 1, #PROGRAMS, 1 do
    if PROGRAMS[i] ~= nil then
      tempNames[i] = PROGRAMS[i][1]
    end
  end
  
  return tempNames
end

function lpkgdb.filelist(package)
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- NAME PARSING:
  tempNames = {}
  for i = 1, #PROGRAMS, 1 do
    if PROGRAMS[i] ~= nil then
      tempNames[i] = PROGRAMS[i][1]
    end
  end
  
  -- CHECK IF THE GIVEN PACKAGE NAME EXISTS:
  packageExists = false
  packageNumber = 0
  for i = 1, #tempNames, 1 do
    if tempNames[i] == package then
      packageExists = true
      packageNumber = i
    end
  end
  
  -- RETURN THE FILE LIST IF EXISTS:
  if packageExists == true then
    return PROGRAMS[packageNumber][4]
  else
    return false
  end
end

function lpkgdb.getver(package)
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- NAME PARSING:
  tempNames = {}
  for i = 1, #PROGRAMS, 1 do
    if PROGRAMS[i] ~= nil then
      tempNames[i] = PROGRAMS[i][1]
    else
      tempNames[i] = false
    end
  end
  
  -- CHECK IF THE GIVEN PACKAGE NAME EXISTS:
  packageExists = false
  packageNumber = 0
  for i = 1, #tempNames, 1 do
    if tempNames[i] == package then
      packageExists = true
      packageNumber = i
    end
  end
  
  -- RETURN THE PACKAGE VERSION IF EXISTS:
  if packageExists == true then
    return PROGRAMS[packageNumber][2]
  else
    return false
  end
end

function lpkgdb.getdesc(package)
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- NAME PARSING:
  tempNames = {}
  for i = 1, #PROGRAMS, 1 do
    if PROGRAMS[i] ~= nil then
      tempNames[i] = PROGRAMS[i][1]
    else
      tempNames[i] = nil
    end
  end
  
  -- CHECK IF THE GIVEN PACKAGE NAME EXISTS:
  packageExists = false
  packageNumber = 0
  for i = 1, #tempNames, 1 do
    if tempNames[i] == package then
      packageExists = true
      packageNumber = i
    end
  end
  
  -- RETURN THE PACKAGE DESCRIPTION IF EXISTS:
  if packageExists == true then
    return PROGRAMS[packageNumber][3]
  else
    return false
  end
end

function lpkgdb.delete(package)
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- SAVE THE OLD DATABASE INFORMATION:
  local currentDatabase = fs.read("C:/lpkg/program.db")
  
  -- SEARCH AND DESTROY THE DATABASE ENTRY:
  tempNames = {}
  for i = 1, #PROGRAMS, 1 do
    if PROGRAMS[i] ~= nil then
      tempNames[i] = PROGRAMS[i][1]
    end
  end
  
  pkgExists = false
  pkgNumber = 0
  files = {}
  version = {}
  for i = 1, tempNames, 1 do
    if tempNames[i] == package then
      pkgExists = true
      pkgNumber = i
      files = PROGRAMS[i][4]
      version = PROGRAMS[i][2]
    end
  end
  
tempfiles = ""
  for i = 1, #files, 1 do
    if i ~= #files then
      tempfiles = tempfiles.."\""..files[i].."\""..","
    elseif i == #files then
      tempfiles = tempfiles..files[i]
    end
  end
  
  tempversion = ""
  for i = 1, 3, 1 do
    if i ~= #version then
      tempversion = tempversion..tostring(version[i])..","
    elseif i == #version then
      tempversion = tempversion..tostring(version[i])
    end
  end
  
  if pkgExists == true then
    local dataToWrite = currentDatabase.gsub("{\""..PROGRAMS[pkgNumber][1].."\",{"..tempversion.."},\""..PROGRAMS[pkgNumber][3]"\",{"..tempfiles.."}}","nil")
    fs.write("C:/lpkg/program.db",dataToWrite)
    return true
  else
    return false
  end
end

function lpkgdb.add(name,version,description,files)
  -- LOAD THE DATABASE:
  fs.write("C:/lpkg/program.db.tempload.lua",fs.read("C:/lpkg/program.db").."return PROGRAMS")
  dofile("C:/lpkg/program.db.tempload.lua")
  fs.delete("C:/lpkg/program.db.tempload.lua")
  
  -- SAVE OLD DATABASE INFORMATION:
  local currentDatabase = fs.read("C:/lpkg/program.db")
  
  -- PROCCESS THE INFORMATION TO WRITE:
  tempfiles = ""
  for i = 1, #files, 1 do
    if i ~= #files then
      tempfiles = tempfiles.."\""..files[i].."\""..","
    elseif i == #files then
      tempfiles = tempfiles..files[i]
    end
  end
  
  tempversion = ""
  for i = 1, 3, 1 do
    if i ~= #version then
      tempversion = tempversion..tostring(version[i])..","
    elseif i == #version then
      tempversion = tempversion..tostring(version[i])
    end
  end
  -- GENERATE THE INFORMATION TO WRITE:
  local packageHeader = "PROGRAMS["..tostring(#PROGRAMS).."] = {\""..name.."\",{"..tempversion.."},\""..description.."\",{"..tempfiles.."}}"
  
  -- WRITE THE PACKAGE HEADER:
  fs.write("C:/lpkg/program.db",currentDatabase..packageHeader.."\n")
end

return lpkgdb