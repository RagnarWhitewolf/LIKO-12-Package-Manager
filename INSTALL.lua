-- LPKG INSTALLER 1.5.0
term = require("terminal")
local FolderPath = "C:/lpkg/"
local args = {...}
--------------------------
if args[1] ~= "--SILENT" then -- NORMAL MODE:

 color(12) print("INSTALLING LIKO-12 PACKAGE MANAGER") color(7)
 sleep(1)
 color(9) print("[WARN] INSTALLING CORE") color(7)
 sleep(0.3)
 fs.newDirectory(FolderPath)
 fs.write(FolderPath.."CORE.lua",fs.read(term.getpath().."CORE.lua"))
 fs.write(FolderPath.."CORE.PKGDB.lua",fs.read(term.getpath().."LPKGDB.lua"))
 fs.write("C:/Programs/System/lpkg.lua",fs.read(term.getpath().."LPKG.lua"))
 fs.write("C:/Programs/System/lpget.lua",fs.read(term.getpath().."LPGET.lua"))
 color(11) print("[OK] CORE INSTALLED") color(7)
 color(9) print("[WARN] CREATING FOLDERS") color(7)
 sleep(0.3)
 fs.newDirectory(FolderPath.."uninstall")
 fs.newDirectory(FolderPath.."manual")
 fs.newDirectory(FolderPath.."license")

 fs.newDirectory(FolderPath.."lib")
 color(11) print("[OK] FOLDERS CREATED") color(7)
 color(9) print("[WARN] FINISHING") color(7)
 sleep(0.3)
 fs.write("C:/lpkg/uninstall/lpkg-CORE",fs.read(term.getpath().."UNINSTALL.lua"))
 fs.write("C:/lpkg/manual/lpkg-CORE",fs.read(term.getpath().."MANUAL.txt"))
 fs.write("C:/lpkg/license/lpkg-CORE",fs.read(term.getpath().."LICENSE.txt"))
 fs.write("C:/lpkg/changelog.txt",fs.read(term.getpath().."CHANGELOG.txt"))

 if fs.exists("C:/lpkg/repo.cfg") == false then
  fs.write("C:/lpkg/repo.cfg",fs.read(term.getpath().."REPO.cfg"))
 end

 fs.write(FolderPath.."lib/lpfs.lua",fs.read(term.getpath().."LPFS.lua"))
 fs.write(FolderPath.."lib/wget.lua",fs.read(term.getpath().."WGET.lua"))
 
 dofile("C:/lpkg/CORE.PKGDB.lua")
 lpkgdb.build()
 
 print("")
 color(11) print("[OK] FINISHED!") color(7)

elseif args[1] == "--SILENT" then -- SILENT MODE:

 fs.newDirectory(FolderPath)
 fs.write(FolderPath.."CORE.lua",fs.read(term.getpath().."CORE.lua"))
 fs.write(FolderPath.."CORE.PKGDB.lua",fs.read(term.getpath().."LPKGDB.lua"))
 fs.write("C:/Programs/System/lpkg.lua",fs.read(term.getpath().."LPKG.lua"))
 fs.write("C:/Programs/System/lpget.lua",fs.read(term.getpath().."LPGET.lua"))
 fs.newDirectory(FolderPath.."uninstall")
 fs.newDirectory(FolderPath.."manual")
 fs.newDirectory(FolderPath.."license")
 fs.newDirectory(FolderPath.."lib")
 fs.write("C:/lpkg/uninstall/lpkg-CORE",fs.read(term.getpath().."UNINSTALL.lua"))
 fs.write("C:/lpkg/manual/lpkg-CORE",fs.read(term.getpath().."MANUAL.txt"))
 fs.write("C:/lpkg/license/lpkg-CORE",fs.read(term.getpath().."LICENSE.txt"))
 fs.write("C:/lpkg/changelog.txt",fs.read(term.getpath().."CHANGELOG.txt"))
 if fs.exists("C:/lpkg/repo.cfg") == false then
  fs.write("C:/lpkg/repo.cfg",fs.read(term.getpath().."REPO.cfg"))
 end
 fs.write(FolderPath.."lib/lpfs.lua",fs.read(term.getpath().."LPFS.lua"))
 fs.write(FolderPath.."lib/wget.lua",fs.read(term.getpath().."WGET.lua"))
 dofile("C:/lpkg/CORE.PKGDB.lua")
 lpkgdb.build()
end