local args = {...} -- COLLECT THE ARGUMENTS

dofile("C:/lpkg/CORE.lua") -- LOAD THE CORE LIBRARY
--------------------------------
if #args == 2 and args[1] == "install" then -- BEGIN INSTALLING PROCESS
  lpkg.get.install(args[2])
elseif #args == 2 and args[1] == "list" and args[2] == "repo" then -- REPOSITORY LISTING
  lpkg.get.getRepositoryListing(true)
elseif #args == 2 and args[1] == "list" then -- PACKAGE REPOSITORY LISTING
  lpkg.get.getRepositoryAppList(args[2],true)
elseif #args == 4 and args[1] == "add" and args[2] == "repo" then -- REPOSITORY ADDING MODE
  lpkg.get.addRepository(args[3],args[4],true)
elseif #args == 3 and args[1] == "delete" and args[2] == "repo" then -- REPOSITORY DELETING MODE
  lpkg.get.deleteRepository(args[3],true)
elseif #args < 1 or args[1] == "-h" or "--help" or "-?" then -- HELP SCREEN
  lpkg.get.help("lpget")
else
  color(8); print("UNKNOWN COMMAND!"); color(7)
end