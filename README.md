<img src="https://pixelreaper.github.io/LIKO-12-Package-Manager/img/LPKG-LOGO.png">

## LIKO-12 PACKAGE MANAGER (lpkg)

[![](https://img.shields.io/badge/LICENSE-APL-blue.svg?style=flat-square)](https://gitea.com/PixelReaper_/LIKO-12-Package-Manager/raw/branch/master/LICENSE.txt "license")
[![](https://img.shields.io/badge/VERSION-1.5.0-yellow.svg?style=flat-square)](https://gitea.com/PixelReaper_/LIKO-12-Package-Manager/releases "version")

This program is a package manager like apt-get and dpkg of debian distributions for LIKO-12 Fantasy Computer.

## "lpkg" INSTRUCTIONS:
* To install a package, type "lpkg install (packagefile)" and  the program will start installing the package on your system.

* To uninstall a package, type "lpkg uninstall (packagename)" and the program will start deleting the entire program of your system.

* If you ignore the package’s name and you want to uninstall it, type "lpkg list" and the program will print the list of all the packages installed on your system.

* To view the manual (if the package have one) of any package just type "lpkg manual (packagename)" and the program will print the manual included in the package.

* To view the lpkg's changelog, type "lpkg changelog" and the program will open the editor with the changelog.


## "lpget" INSTRUCTIONS:
* To install a package, type "lpkg-get (or lpget) install (packagename)" and the program will start downloading the package and installing it.

* If you ignore the packages that are placed in a repository, type "lpkg-get (or lpget) list (repository name)" and the program will open you the editor with the list of this repository.

* But if you don't know what repos are in your system, type this "lpkg-get (or lpget) list repo" and the program will print you the list of the repos that you have added.

* To add a repository, type "lpkg-get (or lpget) add repo (name of repository) (URL of repository)" and the program will add the repository to the configuration file.

* To delete a repository, type "lpkg-get (or lpget) delete repo (repository name)" and the program will read its config. file and will delete the repository that you specify between brackets(repository name).


## NOTES:
If you want to setup a repository or create a package go to SAMPLES folder.

## CREDITS:
* Thanks to Kristina Bernal to check the errors in this documentation.
